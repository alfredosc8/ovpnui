#!/bin/bash

echo "Exporting new client certificate info ${CA_CLIENTS_DIR}"

# Create RSA key
$OPENSSL_BIN genrsa -out "${CA_CLIENTS_DIR}/${CLIENT_NAME}.key" ${KEY_BITSIZE} -aes256

# Create the sign-request
$OPENSSL_BIN req -new -key "${CA_CLIENTS_DIR}/${CLIENT_NAME}.key" -out "${CA_CLIENTS_DIR}/${CLIENT_NAME}.reg" -extensions v3_client -batch -subj "/C=$KEY_COUNTRY/ST=$KEY_PROVINCE/L=$KEY_CITY/O=$KEY_ORG/OU=$KEY_OU/CN=$CLIENT_NAME/emailAddress=$KEY_EMAIL"

# Sign the certificate
$OPENSSL_BIN x509 -req -days ${CERTIFICATE_LIFETIME} -extfile $OPENSSL_CONF -extensions v3_client -in "${CA_CLIENTS_DIR}/${CLIENT_NAME}.reg" -CA "${CA_COMMON_DIR}/ca.cer" -CAkey "${CA_COMMON_DIR}/ca.key" -CAcreateserial -out "${CA_CLIENTS_DIR}/${CLIENT_NAME}.cer" -extensions v3_client -passin pass:"$CA_PASSWORD"

# Export into a pkcs12 bundle
$OPENSSL_BIN pkcs12 -password pass: -export -in "${CA_CLIENTS_DIR}/${CLIENT_NAME}.cer" -inkey "${CA_CLIENTS_DIR}/${CLIENT_NAME}.key" -certfile "${CA_COMMON_DIR}/ca.cer" -out "${CA_CLIENTS_DIR}/${CLIENT_NAME}.p12"

$OPENSSL_BIN gendh -out "${CA_CLIENTS_DIR}/${CLIENT_NAME}.dh" $DH_KEYSIZE
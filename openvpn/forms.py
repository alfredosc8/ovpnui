from django import forms

from choices import COUNTRY_CHOICES

class create_new_server_form(forms.Form):
    domain = forms.CharField(label='Domain', max_length=100, required=True)
    country = forms.ChoiceField(choices = COUNTRY_CHOICES, required = True)
    province = forms.CharField(label='Province', max_length=100, required=True)
    city = forms.CharField(label='City', max_length=100, required=True)
    orginization = forms.CharField(label='Organization', max_length=100, required=True)
    orginizational_unit = forms.CharField(label='Organizational unit', max_length=100, required=True)
    email = forms.EmailField(label='Email', max_length=100, required=True)

    port = forms.IntegerField(label='Port', initial=1194, required=True)
    name = forms.CharField(label='Server Name', max_length=100, required=True)
    key_size = forms.IntegerField(label='Key Size', initial=4096, required=True)
    dh_size = forms.IntegerField(label='DH Size', initial=2048, required=True)

class create_new_client_form(forms.Form):
    name = forms.CharField(label='Server Name', max_length=100, disabled=True, required=True)
    client_name = forms.CharField(label='Server Name', max_length=100, required=True)

    key_size = forms.IntegerField(label='Key Size', initial=4096, required=True)
    dh_size = forms.IntegerField(label='DH Size', initial=2048, required=True)